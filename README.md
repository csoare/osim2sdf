# osim2sdf

OSIM to SDF musculoskeletal model converter

This tool aims at converting musculoskeletal models from Stanford's OSIM format to the [Open Source Robotics Founddation](https://www.openrobotics.org/) [SDF](http://sdformat.org/) format.

## Usage

To convert an OSIM file to an SDF one, simply run the script found in the `src` directory:

```sh
python3 osim2sdf.py input.osim -c config.ini
```

where 
- `input.osim` is the source OSIM file
- `config.ini` is a configuration file describing the conversion properties regarding collisions, joints and muscles. It can be given using either `-c` or `--config`. If not specified, the conversion will include all collisions, joints and muscles

The tool will create in the current working directory two files:
- **output.sdf** an SDF file containintg the skeleton model (links and joints)
- **output.osim** a separate OSIM file containing muscle information

The configuration file has 7 sections marked with square brackets:
- `[general]` can hold the following properties:
	- the `path` to the meshes directory for the script to check their existence when adding the visuals to the SDF file 
	- the `model`  name which is `model` by default
	- the global `pose` which can translate and rotate the whole model
- `[collisions]` holds binary values describing the activation of collisions for a specific body. Each body, can be specified ona new line using the `body = true` syntax. The `default` body will set the collision for all unspecified bodies
- `[joints]` holds values describing each individual joint. Each joint can be set to either `fixed`, `free` to enable it or `none` to remove it. As with collisions, the `default` joint sets the property for the unspecified joints. The `free` keyword can also be configured to lock certain degrees of freedom upon converson using the `free/tff` syntax, where `t` enables and `f` locks the degree
- `[muscles]` is similar to the collisions property. It enables or disables individual muscles using binary values. The `default` muscle sets the rule for the unspecified muscles. Furthermore, path points can be removed from a muscle definition by appending their name with a preceding minus. Wraps can be added by appending their name with a preceding plus (e.g. `MuscleName=true -PathPointName +WrappingPointName`).
- `[wraps]` holds wrapping points definitions.  They can be specified using the syntax `BodyName=WrapType(WrapName, properties)`, where `Bodyname` is the name of the bone onto which the wrap is attached.
- `[coordinates]` defines the value a body coordinate will be assigned to. This is useful for tuning a custom moving path point to fixed path point conversion
- `[alias]` defines alias names for muscles. This is useful for referring said muscles within the muscle control scripts, should they use different declarations in their configuration.

### vtp2dae

The `src` directory also contains the **vtp2dae** script for converting body meshes from VTP to the DAE format used by the SDF file. To use it simply run:

```sh
./vtp2dae.sh input output
```

where `input` is the directory containing the VTP mesh files and `output` is the destination directory where the `DAE` conversions will be placed. Each `DAE` mesh will be joined by a collision box mesh (`.col.dae`).

## roscon

ROScon is a python module which allows activating the muscles declared in the OSIM files. The ROSController constructor accepts a list of muscle strings representing the muscles of interest which are to be controlled. The `activate` method then allows applying a force on a given muscle. The class also provides a `topics` dictionary showing the ROS topic for each muscle.

## Resources

The `resources` directory contains sample OSIM body models together with their necessary body meshes. The resources have been extracted from the [OpenSim](http://opensim.stanford.edu/) software.
