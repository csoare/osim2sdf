import sys
import vtk

reader = vtk.vtkXMLPolyDataReader()
reader.SetFileName(sys.argv[1])
reader.Update()

writer = vtk.vtkSTLWriter()
writer.SetInputConnection(reader.GetOutputPort())
writer.SetFileName(sys.argv[2])
writer.Write()
