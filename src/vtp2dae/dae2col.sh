#!/bin/bash

rm $1/*.col.dae
if [ -z $2 ]; then
	splits=1
else
	splits=$2
fi

for i in $(ls $1); do
    if [ "${i##*.}" != 'dae' ]; then
        continue
    fi
    
    dae="$1/$i"
    col="$1/${i%.*}.col.dae"
    python dae2col.py $dae $col $splits

    echo $col in $splits done =======================================
done

