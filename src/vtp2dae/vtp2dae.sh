#!/bin/bash

mkdir -p $2
if [ -z $3 ]; then
    splits=1
else
    splits=$3
fi

for i in $(ls $1); do
    if [ "${i##*.}" != 'vtp' ]; then
        continue
    fi

    vtp=$1/$i
    stl="/tmp/${i%.*}.stl"
    dae="$2/${i%.*}.dae"
    col="$2/${i%.*}.col.dae"
    
    python3 vtp2stl.py $vtp $stl
    blender --background --python stl2dae.py -- $stl $dae
    python3 dae2col.py $dae $col $splits
    
    rm $stl
    echo $dae done =======================================
done
