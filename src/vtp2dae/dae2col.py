import sys
import numpy as np
from collada import *

inmesh = Collada(sys.argv[1])
            
vert = []
for geom in inmesh.scene.objects('geometry'):
    for prim in geom.primitives():
        vert.extend(prim.vertex.tolist())

vert = np.array(vert)
minvert, maxvert = vert.min(axis=0), vert.max(axis=0)

mesh = Collada()
effect = material.Effect("effect0", [], "phong", diffuse=(1,0,0), specular=(0,1,0))
mat = material.Material("material0", "mymaterial", effect)
mesh.effects.append(effect)
mesh.materials.append(mat)

minvert[1] = -minvert[1]
maxvert[1] = -maxvert[1]

class Builder:
    def __init__(self):
        self.vert = []
        self.indices = []
        self.vind = {}

    def triangle(self, a, b, c):
        for x in (a, b, c):
            if x not in self.vind:
                self.vert.extend(x)
                self.vind[x] = len(self.vert) / 3 - 1
            self.indices.append(self.vind[x])

    def square(self, (a, b, c, d)):
        self.triangle(a, b, d)
        self.triangle(a, d, c)

    def tile(self, (lx, ly, lz)):
        v = []
        for x in lx:
            for y in ly:
                for z in lz:
                    v.append((x, y, z))
        self.square(tuple(v))

    def face(self, frees, lims=[]):
        lims = lims[:]
        if len(lims) == 3:
            self.tile(tuple(lims))
            return

        if type(frees[0]) == list:
            lims.append(frees[0])
            self.face(frees[1:], lims)
        else:
            l = [minvert[len(lims)], maxvert[(len(lims))]]
            if frees[0] < 0:
                l.reverse()

            step = (l[1] - l[0]) / int(sys.argv[3])
            lims.append([None,None])
            for x in np.arange(l[0], l[1], step):
                lims[-1] = [x, x+step]
                self.face(frees[1:], lims)



    def box(self):
        self.face([-1, 1, [minvert[2]]])
        self.face([-1, -1, [maxvert[2]]])
        self.face([1, [minvert[1]], 1])
        self.face([-1, [maxvert[1]], 1])
        self.face([[minvert[0]], -1, 1])
        self.face([[maxvert[0]], 1, 1])

builder = Builder()
builder.box()
vert_src = source.FloatSource("cubeverts-array", np.array(builder.vert), ('X', 'Y', 'Z'))

geom = geometry.Geometry(mesh, "geometry0", "mycube", [vert_src])

input_list = source.InputList()
input_list.addInput(0, 'VERTEX', "#cubeverts-array")

triset = geom.createTriangleSet(np.array(builder.indices), input_list, "materialref")
geom.primitives.append(triset)
mesh.geometries.append(geom)

matnode = scene.MaterialNode("materialref", mat, inputs=[])
geomnode = scene.GeometryNode(geom, [matnode])
node = scene.Node("node0", children=[geomnode])

myscene = scene.Scene("myscene", [node])
mesh.scenes.append(myscene)
mesh.scene = myscene

mesh.write(sys.argv[2])
