#!/usr/bin/python

import time
import argparse
import numpy as np
import matplotlib.pyplot as plt
      
header = ['RF', 'VL', 'VM', 'ST', 'BF', 'MG', 'TA']
def activate(forces):
    return list(zip(header, forces / forces.max()))


parser = argparse.ArgumentParser(description='ROS Muscles Controller')
parser.add_argument('muscles', help='path to file containing muscles of interest')
parser.add_argument('forces', help='computed forces')
parser.add_argument('-r', '--run', help='run trial without user input', action='store_true')
parser.add_argument('-t', '--ttrial', type=float, default=5, help='trial duration in seconds')
parser.add_argument('-d', '--debug', help='Plot forces, no simulation', action='store_true')
parser.add_argument('-n', '--nonc', help='No ncurses', action='store_true')
args = parser.parse_args()

if not args.nonc:
    import curses
    stdscr = curses.initscr()
    curses.noecho()
    curses.cbreak()

def nprint(i, s):
    if not args.nonc:
        stdscr.addstr(i, 0, s)
        stdscr.clrtoeol()
        stdscr.refresh()
    else:
        print(s)

class ForcePlotter:
    def __init__(self):
        axes = plt.gca()
        axes.set_xlim(0, args.ttrial-1)
        axes.set_ylim(0, 1)

        plt.xlabel('Time (s)')
        plt.ylabel('Muscle force (scaled)')
        plt.title('Muscle activity over time')

        self.lines = []
        for x in range(len(header)):
            line, = plt.plot([], [], '-o')
            self.lines.append(line)
        plt.legend(header)

    def reset(self, forces):
        self.forces = forces.T
        self.step(-1)

    def step(self, i):
        i += 1
        for x in zip(self.forces, self.lines):
            x[1].set_xdata(np.arange(0, i))
            x[1].set_ydata(x[0][:i])
        plt.draw()
        plt.pause(1e-17)

try:
    if not args.debug:
        from roscon import ROSControl
        con = ROSControl(args.muscles)
    tmod = con if not args.debug else time
    forces = np.load(args.forces)
    ttrial = args.ttrial

    forces /= forces.max()
    fplot = ForcePlotter()
    for i in range(len(forces)):
        nprint(i, 'Trial %d' % (i+1))
        granule = ttrial / len(forces[i])
        fplot.reset(forces[i])

        if args.run:
            tmod.sleep(1)

        for j in range(len(forces[i])):
            nprint(i+1, 'Second %.2f' % (j*granule))
            fplot.step(j)
            if not args.debug:
                con.activate_multiple(activate(forces[i][j]))

            if args.run:
                tmod.sleep(granule)
            else:
                stdscr.getkey()

        if not args.nonc:
            nprint(i+1, 'Continue ?')
            c = stdscr.getch()
            if c == ord('n'):
                break
finally:
    if not args.nonc:
        curses.nocbreak()
        curses.echo()
        curses.endwin()
