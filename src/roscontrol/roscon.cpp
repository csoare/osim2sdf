#include <boost/python.hpp>
#include <chrono>
#include <thread>
#include <string>
#include <fstream>
#include <regex>
#include <algorithm>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string.hpp>

#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <std_msgs/Float64.h>
#include <gazebo_msgs/GetLinkState.h>
#include <gazebo_msgs/GetJointProperties.h>
#include <gazebo_msgs/ApplyJointEffort.h>

struct JointProperties {
	boost::python::list position, rate;
};

class ROSControl {
public:
	ROSControl(const std::string& model, const boost::python::list& muscles)
		: model(model)
	{
		initMuscles(muscles);
		initTopics();
		initROS();
	}

	ROSControl(const std::string& conf)
	{
		namespace bpt = boost::property_tree;
		bpt::ptree pt;
		bpt::read_ini(conf, pt);

		initMuscles(pt);
		initJointAlterations(pt);
		initTopics();
		initROS();
		initMusclesAliases(pt);
	}

	void activate(const std::string& muscle, double force) const
	{
		if (pubs.find(muscle) == pubs.end())
			throw std::runtime_error("Muscle \"" + muscle + "\" not found");

		std_msgs::Float64 msg;
		msg.data = force;

		for (const auto& x : pubs.at(muscle))
			x->publish(msg);
	}

	void activateMultiple(const boost::python::list& forces) const
	{
		namespace py = boost::python;
		for (int i = 0; i < py::len(forces); i++) {
			const py::tuple& x = py::extract<py::tuple>(forces[i]);
			activate(py::extract<std::string>(x[0])(), py::extract<double>(x[1])());
		}
	}

	void resetMuscles() const
	{
		std_msgs::Float64 msg;
		msg.data = 0;
		
		for (const auto& t : topics)
			pubs.at(t.second[0])[0]->publish(msg);
	}

	void alias(const std::string& from, const std::string& to)
	{
		if (pubs.find(from) == pubs.end())
			throw std::runtime_error("Muscle \"" + from + "\" not found");

		for (const auto& x : pubs[from]) {
			pubs[to].push_back(x);
			topics[x->getTopic()].push_back(to);
		}
	}

	boost::python::dict getAliases() const
	{
		namespace py = boost::python;
		py::dict dt;

		for (const auto& topic : topics) {
			py::list l;

			for (const auto& alias : topic.second)
				l.append(alias);
			dt[topic.first] = l;
		}
		return dt;
	}

	boost::python::list getTopics() const
	{
		boost::python::list ret;
		for (const auto& x : muscles)
			ret.append(pubs.at(x)[0]->getTopic());
		return ret;
	}

	JointProperties getJointProperties(const std::string& joint) const
	{
		gazebo_msgs::GetJointProperties jprop;
		jprop.request.joint_name = joint;

		callServiceClient<gazebo_msgs::GetJointProperties>("/gazebo/get_joint_properties", jprop);

		if (jointAlterations.find(joint) != jointAlterations.end()) {
			if (jprop.response.position.size() != jointAlterations.at(joint).size()) {
				std::ostringstream oss;
				oss << "Joint alteration vector length is " << jointAlterations.at(joint).size() 
					<< "; expected " << jprop.response.position.size();
				throw std::runtime_error(oss.str());
			}

			std::transform(
				jprop.response.position.begin(), jprop.response.position.end(),
				jointAlterations.at(joint).begin(), 
				jprop.response.position.begin(), 
				std::plus<double>());
		}

		JointProperties ret;
		for (const auto& x : jprop.response.position)
			ret.position.append(x);
		for (const auto& x : jprop.response.rate)
			ret.rate.append(x);
		
		return ret;
	}

	void setJointEffort(const std::string& joint, double effort, double start=0, double duration=100)
	{
		gazebo_msgs::ApplyJointEffort jeff;

		jeff.request.joint_name = joint;
		jeff.request.effort = effort;
		jeff.request.start_time = ros::Time(start);
		jeff.request.duration = ros::Duration(duration);

		callServiceClient<gazebo_msgs::ApplyJointEffort>("/gazebo/apply_joint_effort", jeff);
	}

	void sleep(double duration) const
	{
		ros::Duration(duration).sleep();
	}

	double time() const
	{
		return ros::Time::now().toSec();
	}

	void resetSimulation() const
	{
		std_srvs::Empty resetSim;
		ros::service::call("/gazebo/reset_sim", resetSim);
	}

private:
	template <typename T>
	void callServiceClient(const std::string& s, T& subj) const
	{
		ros::NodeHandle node;
		auto serviceClient = node.serviceClient<T>(s);
		if (!serviceClient.call(subj))
			throw std::runtime_error("Could not call " + s + " service");

		if (!subj.response.success)
			throw std::runtime_error("Call for " + s + " not successful");
	}

	void initMuscles(const boost::python::list& muscles)
	{
		for (int i = 0; i < boost::python::len(muscles); i++)
			this->muscles.push_back(boost::python::extract<std::string>(muscles[i]));
	}

	void initMuscles(const boost::property_tree::ptree& pt)
	{
		model = pt.get_child("general").get<std::string>("model");
		for (const auto& x : pt.get_child("alias"))
			muscles.push_back(x.first);
	}

	void initJointAlterations(const boost::property_tree::ptree& pt)
	{
		const std::string regalter = "(-?[0-9]+\\.[0-9]* *)+";
		const std::regex exall("^fixed(/" + regalter + ")?$|^free(/[t|f]+)?(/" + regalter + ")?$");
		const std::regex exnum("^" + regalter + "$");

		for (const auto& x : pt.get_child("joints")) {
			if (x.first == "default")
				continue;

			auto jconf = x.second.get_value<std::string>();
			if (!std::regex_match(jconf, exall))
				throw std::runtime_error("Invalid joint configuration: " + jconf);

			std::vector<std::string> splits;
			boost::split(splits, jconf, boost::is_any_of("/"));
			if (!std::regex_match(splits.back(), exnum))
				continue;

			try {
				double angle;
				std::istringstream iss(splits.back());
				while (iss >> angle)
					jointAlterations[x.first].push_back(angle);
			} catch (...) {
				throw std::runtime_error("Invalid joint alteration: " + splits.back());
			}
		}
	}

	void initTopics()
	{
		for (const auto& muscle : muscles) {
			std::ostringstream oss;
			oss << "/gazebo_muscle_interface/" << model << "/" << muscle << "/cmd_activation";
			topics[oss.str()].push_back(muscle);
		}
	}

	void initROS()
	{
		if (!rosinit) {
			int argc = 0;
			ros::init(argc, nullptr, "cppcontroller", ros::init_options::AnonymousName);
			rosinit = true;
		}

		ros::NodeHandle node;
		for (const auto& i : topics)
			pubs[i.second[0]].emplace_back(std::make_shared<ros::Publisher>(node.advertise<std_msgs::Float64>(i.first, 10)));
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}

	void initMusclesAliases(const boost::property_tree::ptree& pt)
	{
		for (const auto& x : pt.get_child("alias"))
			alias(x.first, x.second.get_value<std::string>());
	}

	static bool rosinit;
	std::string model;
	std::vector<std::string> muscles;
	std::map<std::string, std::vector<std::string>> topics;
	std::map<std::string, std::vector<std::shared_ptr<ros::Publisher>>> pubs;
	std::map<std::string, std::vector<double>> jointAlterations;
};

bool ROSControl::rosinit = false;

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(ROSControlOverloads, setJointEffort, 2, 4);

BOOST_PYTHON_MODULE(roscon) {
	using namespace boost::python;

	class_<JointProperties>("JointProperties", init<>())
		.add_property("position", &JointProperties::position)
		.add_property("rate", &JointProperties::rate)
		;

	class_<ROSControl>("ROSControl", init<const std::string&, const list&>())
		.def(init<const std::string&>())
		.def("activate", &ROSControl::activate, "Apply force to muscle")
		.def("activate_multiple", &ROSControl::activateMultiple, "Apply forces to muscles")
		.def("joint_properties", &ROSControl::getJointProperties, "Get joint properties")
		.def("set_joint_effort", &ROSControl::setJointEffort, ROSControlOverloads())
		.def("reset_sim", &ROSControl::resetSimulation, "Reset simulation")
		.def("reset_mus", &ROSControl::resetMuscles, "Reset all muscles to 0 activation")
		.def("alias", &ROSControl::alias, "Assign alias to muscle")
		.add_property("aliases", &ROSControl::getAliases, "Get used topics and their muscle aliases")
		.add_property("topics", &ROSControl::getTopics, "Get used topics")

		.def("sleep", &ROSControl::sleep, "Sleep for duration in seconds")
		.add_property("time", &ROSControl::time, "Get current time in seconds")
		;
}
