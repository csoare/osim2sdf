#!/usr/bin/python3

import os
import re
import argparse
import configparser as cp
import xml.etree.ElementTree as ET
from xml.dom.minidom import parseString

import numpy as np
from math import sin, cos
from collections import OrderedDict
from scipy.interpolate import interp1d

class OSIMCoordinate:
    def __init__(self, oc, joint):
        self.oc = oc
        self.joint = joint
        
    @property
    def name(self):
        return self.oc.get('name')
        
    @property
    def default_value(self):
        return float(self.oc.find('default_value').text)
        
    @property
    def is_rotational(self):
        return self.oc.find('motion_type').text == 'rotational'
        
    @property
    def range(self):
        return np.array(self.oc.find('range').text.split()).astype('float')

def build_spline(e):
    xs = np.array(e.find('x').text.split()).astype(float)
    ys = np.array(e.find('y').text.split()).astype(float)
    if e.tag == 'SimmSpline' or e.tag == 'PiecewiseLinearFunction':
        return interp1d(xs, ys)
    if e.tag == 'NaturalCubicSpline':
        return interp1d(xs, ys, kind='cubic')
    return None

class OSIMTransformAxis:
    def __init__(self, ost, joint):
        self.ost = ost
        self.joint = joint
        self.__value = self.__absolute_value = None
        
    @property
    def name(self):
        return self.ost.get('name')
        
    @property
    def coordinate(self):
        coord = self.ost.find('coordinates').text.strip()
        return self.joint.coordinates[coord] if coord is not None else None
 
    @property
    def axis(self):
        ax = np.array(self.ost.find('axis').text.split()).astype(float)
        ax[1], ax[2] = -ax[2], ax[1]
        return ax
        
    @property
    def constant(self):
        func = self.ost.find('function').find('Constant')
        return float(func.find('value').text) if func is not None else None
        
    @property
    def linear(self):
        func = self.ost.find('function').find('LinearFunction')
        if func is None:
            return None
        
        coef = list(map(float, func.find('coefficients').text.split()))
        return lambda x: coef[0]*x+coef[1]
        
    @property
    def spline(self):
        return build_spline(list(self.ost.find('function'))[0])
        
    @property
    def value(self):
        if self.__value is None:
            if self.constant is not None:
                self.__value = self.constant
            elif self.linear is not None:
                self.__value = self.linear(self.coordinate.default_value)
            elif self.spline is not None:
                self.__value = self.spline(self.coordinate.default_value)
        return self.__value
        
    @property
    def transform(self):
        return self.axis * self.value

class OSIMVisual:
    def __init__(self, dgeo, body_parent):
        self.dgeo = dgeo
        self.body_parent = body_parent
        self.__collision = None
    
    @property
    def geometry_file(self):
        return os.path.splitext(self.dgeo.find('geometry_file').text)[0].strip()

    @property
    def rotation(self):
        rot = np.array(self.dgeo.find('transform').text.split()[:3]).astype(float)
        rot[1], rot[2] = -rot[2], rot[1]
        return rot

    @property
    def translation(self):
        trans = np.array(self.dgeo.find('transform').text.split()[3:]).astype(float)
        trans[1], trans[2] = -trans[2], trans[1]
        return trans

    @property
    def transform(self):
        return np.concatenate([self.rotation, self.translation])

    def __tosdf(self, tag, ext):
        fname = '%s.%s' % (self.geometry_file, ext)
        if self.body_parent.parent.mesh_dir is not None:
            mesh_path = os.path.join(self.body_parent.parent.mesh_dir, fname)
            if not os.path.isfile(mesh_path):
                print('Warning: mesh', mesh_path, self.geometry_file, 'not found')
        
        vis = ET.Element(tag, name='%s-%s-%s' % (self.body_parent.name, self.geometry_file, tag[:3]))
        ET.SubElement(vis, 'pose').text = np.array2string(self.transform)[1:-1]
        
        geometry = ET.SubElement(vis, 'geometry')
        mesh = ET.SubElement(geometry, 'mesh')
        
        ET.SubElement(mesh, 'uri').text = 'model://%s/meshes/%s' % (self.body_parent.parent.model_name, fname)
        ET.SubElement(mesh, 'scale').text = self.dgeo.find('scale_factors').text
        
        return vis
    
    def collision_tosdf(self):
        return self.__tosdf('collision', 'col.dae')
        
    def visual_tosdf(self):
        return self.__tosdf('visual', 'dae')

class Rotation:
    def __init__(self, rot):
        mx = lambda t: np.array([[1,0,0], [0,cos(t),-sin(t)], [0,sin(t),cos(t)]])
        my = lambda t: np.array([[cos(t),0,sin(t)], [0,1,0], [-sin(t),0,cos(t)]])
        mz = lambda t: np.array([[cos(t),-sin(t),0], [sin(t),cos(t),0], [0,0,1]])

        self.mt = np.identity(3)
        for m in zip([mx, my, mz], rot):
            self.mt = self.mt.dot(m[0](m[1]))

    def apply(self, vec):
        return self.mt.dot(vec)

class OSIMJoint:
    def __init__(self, ojoint, child):
        self.ojoint = ojoint

        self.__child = child
        self.__coordinates = self.__spatial_transform = self.__spatial_transform_labeled = None
        self.__freedom = None
        self.__move_axis = None

    transformations = True
        
    @property
    def name(self):
        return self.ojoint.get('name')
        
    @property
    def parent(self):
        return self.child.parent.bodies[self.ojoint.find('parent_body').text.strip()]
        
    @property
    def child(self):
        return self.__child
        
    @property
    def location_in_parent(self):
        loc = np.array(self.ojoint.find('location_in_parent').text.split()).astype(float)
        loc[1], loc[2] = -loc[2], loc[1]
        return loc
        
    @property
    def orientation_in_parent(self):
        ori = np.array(self.ojoint.find('orientation_in_parent').text.split()).astype(float)
        ori[1], ori[2] = -ori[2], ori[1]
        return ori

    @property
    def coordinates(self):
        if self.__coordinates is None:
            self.__coordinates = {}
            for x in self.ojoint.find('CoordinateSet').find('objects'):
                coord = OSIMCoordinate(x, self)
                self.__coordinates[coord.name] = coord
        return self.__coordinates
        
    @property
    def spatial_transform(self):
        if self.__spatial_transform is None:
            self.__spatial_transform = [OSIMTransformAxis(x, self) for x in 
                                       self.ojoint.find('SpatialTransform')]
        return self.__spatial_transform
        
    @property
    def rotational(self):
        ret = self.orientation_in_parent
        if OSIMJoint.transformations:
            ret += sum([x.transform for x in self.spatial_transform[:3]])
        return ret
        
    @property
    def translational(self):
        ret = self.location_in_parent
        if OSIMJoint.transformations:
            ret += sum([x.transform for x in self.spatial_transform[3:]])
        return ret
        
    @property
    def pose(self):
        return np.concatenate([self.translational, self.rotational])

    def __configure(self):
        manager = self.child.parent.config_joints
        s = manager[self.name] if self.name in manager else manager['default']

        regalter = r'(-?[0-9]+\.[0-9]* *)+'
        if not re.match(r'^fixed(/' +regalter+ r')?$|^free(/[t|f]+)?(/' +regalter+ r')?$', s):
            raise Exception('%s is not a valid joint configuration' % s)
        args = s.split('/')

        faxis = self.nblocked_axis
        if s.startswith('fixed'):
            okfree = False
            self.__freedom = ['f'] * len(faxis)
        elif s.startswith('free'):
            okfree = re.match(r'^[t|f]+$', args[1]) if len(args) > 1 else False
            if okfree:
                if len(args[1]) != len(faxis):
                    raise Exception('Custom joint %s freedom has %d arguments; %d expected' % (self.name, len(args[1]), len(faxis)))
                self.__freedom = list(args[1])
            else:
                self.__freedom = ['t'] * len(faxis)

        if (len(args) > 1 and not okfree) or len(args) == 3:
            fmove = np.array(args[-1].split()).astype(float)
            if len(fmove) != len(faxis):
                raise Exception('Custom joint %s rotation has %d arguments; %d expected' % (self.name, len(fmove), len(faxis)))
            self.__move_axis = fmove
        else:
            self.__move_axis = np.zeros(len(faxis))
        
    @property
    def free_axis(self):
        if self.parent.mass == 0:
            return []

        free = [x[0] for x in zip(self.spatial_transform[:3], self.freedom) 
                if x[0].constant is None and x[1] == 't']
        for x in free:
            if x.linear is None:
                print('Warning:', self.name, x.name, 'is not linear')
                
        return free

    @property
    def free_moves(self):
        if self.parent.mass == 0:
            return []
        return [x[0] for x in zip(self.moves, self.freedom) if x[1] == 't']

    @property
    def freedom(self):
        if self.__freedom is None:
            self.__configure()
        return self.__freedom

    @property
    def moves(self):
        if self.__move_axis is None:
            self.__configure()
        return self.__move_axis

    @property
    def nblocked_axis(self):
        return [x.axis for x in self.spatial_transform[:3] if x.constant is None]

    @property
    def move_rotation(self):
        return sum([x[0]*x[1] for x in zip(self.moves, self.nblocked_axis)])
    
    @property
    def type(self):
        constants = len(self.free_axis)
        types = ['fixed', 'revolute', 'revolute2', 'ball']
        return types[constants]
        
    def tosdf(self):
        sdf = ET.Element('joint', name=self.name, type=self.type)
        ET.SubElement(sdf, 'parent').text = self.parent.name if self.parent.mass > 0 else 'world'
        ET.SubElement(sdf, 'child').text = self.child.name
        
        if self.type.startswith('revolute'):
            for x in zip(self.free_axis, self.free_moves, np.arange(len(self.free_axis))):
                axis = ET.SubElement(sdf, 'axis' if x[2] == 0 else 'axis2')
                ET.SubElement(axis, 'xyz').text =  np.array2string(x[0].axis)[1:-1]
                ET.SubElement(axis, 'use_parent_model_frame').text = 'true'
        
                limit = ET.SubElement(axis, 'limit')
                ET.SubElement(limit, 'lower').text = str(x[0].coordinate.range[0] - x[1])
                ET.SubElement(limit, 'upper').text = str(x[0].coordinate.range[1] - x[1])
                
        return sdf

class OSIMWrap:
    def __init__(self, owrap, parent):
        self.owrap = owrap
        self.parent = parent

    @property
    def xyz_body_rotation(self):
        rot = np.array(self.owrap.find('xyz_body_rotation').text.split()).astype(float)
        rot[1], rot[2] = -rot[2], rot[1]
        return rot

    @property
    def translation(self):
        tra = np.array(self.owrap.find('translation').text.split()).astype(float)
        tra[1], tra[2] = -tra[2], tra[1]
        return tra

    def adapt(self):
        rot = np.array2string(self.xyz_body_rotation)[1:-1]
        tra = np.array2string(self.translation)[1:-1]
        self.owrap.find('xyz_body_rotation').text = rot
        self.owrap.find('translation').text = tra
        self.owrap.find('VisibleObject').find('transform').text = '%s %s' % (rot, tra)

class OSIMBody:
    def __init__(self, ebody, parent):
        self.ebody = ebody
        self.parent = parent
        self.__collision_enabled = False
        self.__joint = None
        self.__absolute_pose = None
        self.__nwraps = []

        for x in self.ebody.find('WrapObjectSet').find('objects'):
            OSIMWrap(x, self).adapt()
        
    @property
    def name(self):
        return self.ebody.get('name')
        
    @property
    def mass(self):
        return float(self.ebody.find('mass').text)

    @property
    def mass_center(self):
        center = np.array(self.ebody.find('mass_center').text.split()).astype(float)
        center[1], center[2] = -center[2], center[1]
        return np.concatenate([center, [0,0,0]])
        
    @property
    def joint(self):
        if self.__joint is None:
            joint = self.ebody.find('Joint').find('CustomJoint')
            self.__joint = OSIMJoint(joint, self) if joint is not None and self.mass > 0 else None
        return self.__joint

    @property
    def wraps(self):
        return [x for x in self.ebody.find('WrapObjectSet').find('objects')] + self.__nwraps

    def compute_pose(self, p):
        if self.joint is None:
            return p
        p += self.joint.pose + np.concatenate(([0,0,0], self.joint.move_rotation))
        pj = self.joint.parent.joint
        p[:3] = Rotation(pj.move_rotation if pj is not None else np.zeros(3)).apply(p[:3])
        return self.joint.parent.compute_pose(p)
        
    @property
    def absolute_pose(self):
        if self.__absolute_pose is None:
            if self.joint is None:
                return None
            self.__absolute_pose = self.compute_pose(np.zeros(6))
        return self.__absolute_pose
        
    def __physical(self, sdf):
        inertial = ET.SubElement(sdf, 'inertial')
        ET.SubElement(inertial, 'mass').text = self.ebody.find('mass').text
        ET.SubElement(inertial, 'pose').text = np.array2string(self.mass_center)[1:-1]

        inertia = ET.SubElement(inertial, 'inertia')
        for x in [('xx', 'xx'), ('yy', 'zz'), ('zz', 'yy'),
                  ('xy', 'xz'), ('xz', 'xy'), ('yz', 'yz')]:
            ET.SubElement(inertia, 'i%s' % x[0]).text = self.ebody.find('inertia_%s' % x[1]).text
    
    def __visual(self, sdf, collision):
        for child in self.ebody.find('VisibleObject').find('GeometrySet').find('objects'):
            visual = OSIMVisual(child, self)
            sdf.append(visual.visual_tosdf())

            if collision if type(collision) == bool else collision == 'true':
                sdf.append(visual.collision_tosdf())
                
    def __pose(self, sdf):
        if self.joint is None:
            return
        ET.SubElement(sdf, 'pose', frame=self.joint.parent.name).text = np.array2string(self.absolute_pose)[1:-1]
    
    def tosdf(self, collision):
        sdf = ET.Element('link')
        sdf.set('name', self.name)
        
        self.__pose(sdf)
        self.__physical(sdf)
        self.__visual(sdf, collision)
        
        return sdf

    def wrap(self, s):
        if not re.match('^WrapCylinder\\([^,]+,([^,=]+=[^,=]+,)*[^,=]+=[^,=]+\\)$', s):
            raise Exception('Wrap object %s for %s invalid', s, self.name)
        
        wobj = ET.Element(s[:s.find('(')], name=s[s.find('(')+1:s.find(',')].strip())
        for prop in s[s.find(',')+1:-1].split(','):
            val = [x.strip() for x in prop.split('=')]
            ET.SubElement(wobj, val[0]).text = val[1]
        ET.SubElement(wobj, 'active').text = 'true'

        self.__nwraps.append(wobj)

    def toosim(self):
        b = ET.Element('Body', name=self.name)
        wset = ET.SubElement(b, 'WrapObjectSet', name='')
        wraps = ET.SubElement(wset, 'objects')
        for x in self.wraps:
            wraps.append(x)            
        ET.SubElement(wset, 'groups')
        return b

class OSIMPathPoint:
    def __init__(self, opp, muscle):
        self.opp = opp
        self.muscle = muscle

        if muscle.parent.debug and self.is_moving:
            print(opp.tag, muscle.name, [(x.name, x.range) for x in self.coordinates])

    @property
    def name(self):
        return self.opp.get('name')

    @name.setter
    def name(self, x):
        self.opp.set('name', x)

    @property
    def location(self):
        loc = np.array(self.opp.find('location').text.split()).astype(float)
        loc[1], loc[2] = -loc[2], loc[1]
        return loc

    @property
    def adapted_location(self):
        if not self.is_moving:
            return self.location
        par = self.muscle.parent
        return np.array([x[0](float(par.movingpp[x[1].name] if x[1].name in par.movingpp else par.coordinates[x[1].name].default_value))
            for x in zip(self.splines, self.coordinates)])

    @property
    def is_moving(self):
        return self.opp.tag == 'MovingPathPoint'

    @property
    def splines(self):
        if not self.is_moving:
            return None
        s = [build_spline(list(x)[0]) for x in self.opp if x.tag.endswith('_location')]
        return [s[0], lambda x: -s[2](x), s[1]]

    @property
    def coordinates(self):
        if not self.is_moving:
            return None
        c = [self.muscle.parent.coordinates[x.text] for x in self.opp if x.tag.endswith('_coordinate')]
        c[1], c[2] = c[2], c[1]
        return c

    def adapt(self):
        self.opp.find('location').text = np.array2string(self.adapted_location)[1:-1]
        self.opp.tag = 'PathPoint'

        blacklist = [x for x in self.opp if x.tag not in ['location', 'body']]
        for x in blacklist:
            self.opp.remove(x)
        
class OSIMMuscle:
    def __init__(self, omusc, parent):
        self.omusc = omusc
        self.parent = parent
        self.__path = None
        
    @property
    def name(self):
        return self.omusc.get('name')

    @property
    def tag(self):
        return self.omusc.tag
        
    @property
    def path(self):
        if self.__path is None:
            self.__path = {}
            for x in self.omusc.find('GeometryPath').find('PathPointSet').find('objects'):
                opp = OSIMPathPoint(x, self)
                if opp.name in self.__path:
                    opp.name = opp.name + '+'
                self.__path[opp.name] = opp
        return self.__path
        
    def adapt(self, use):
        removal = set([x[1:] for x in use.split() if x[0] == '-'])
        ppset = self.omusc.find('GeometryPath').find('PathPointSet').find('objects')
        for point in self.path.values():
            if point.name not in removal:
                point.adapt()
            else:
                ppset.remove(point.opp)

        pwset = self.omusc.find('GeometryPath').find('PathWrapSet').find('objects')
        for wrap in [x[1:] for x in use.split() if x[0] == '+']:
            pw = ET.SubElement(pwset, 'PathWrap', name='%s-%s-pw' % (wrap, self.name))
            ET.SubElement(pw, 'wrap_object').text = wrap
            ET.SubElement(pw, 'method').text = 'midpoint'

class OSIM:
    def __init__(self, fname, config, debug=False):
        self.oroot = ET.parse(fname).getroot()
        self.mesh_dir = config['meshes'] if 'meshes' in config else None
        self.newname = config['model']
        self.debug = debug
        self.omodel = self.oroot.find('Model')
        
        self.sdf = ET.Element('sdf', version='1.5')
        self.model = ET.SubElement(self.sdf, 'model', name=self.model_name)
        if 'pose' in config:
            ET.SubElement(self.model, 'pose').text = config['pose']
        if 'initrans' in config:
            OSIMJoint.transformations = config['initrans'].lower() == 'true'

        self.__bodies = None
        self.__coordinates = None
        self.__muscles = None
        
        self.osim = ET.Element('OpenSimDocument', Version=self.oroot.get('Version'))
        self.osim_model = ET.SubElement(self.osim, 'Model')
        self.osim_bodies = ET.SubElement(self.osim_model, 'BodySet')
        self.osim_bodies = ET.SubElement(self.osim_bodies, 'objects')
        self.osim_muscles = ET.SubElement(self.osim_model, 'ForceSet')
        self.osim_muscles = ET.SubElement(self.osim_muscles, 'objects')
        
    @property
    def model_name(self):
        return self.newname if self.newname is not None else self.omodel.get('name')
        
    @property
    def bodies(self):
        if self.__bodies is None:
            self.__bodies = OrderedDict()
            bodyset = self.omodel.find('BodySet')
            for child in bodyset.find('objects'):
                body = OSIMBody(child, self)
                self.__bodies[body.name] = body
        return self.__bodies

    @property
    def coordinates(self):
        if self.__coordinates is None:
            self.__coordinates = {}
            for body in self.bodies.values():
                if body.joint is not None:
                    self.__coordinates.update(body.joint.coordinates)
        return self.__coordinates
       
    def build_body(self, collisions, wraps, joints):
        self.model.append(ET.Comment('============================== Links ===================================='))
        for key, val in wraps.items():
            self.bodies[key].wrap(val)

        self.config_joints = joints
        for body in self.bodies.values():
            if body.mass != 0:
                self.model.append(body.tosdf(collisions[body.name] if body.name in collisions else collisions['default']))
                if len(body.wraps) > 0:
                    self.osim_bodies.append(body.toosim())
            
    @property
    def muscles(self):
        if self.__muscles is None:
            self.__muscles = OrderedDict()
            muscleset = self.omodel.find('ForceSet')
            for child in muscleset.find('objects'):
                muscle = OSIMMuscle(child, self)
                if 'muscle' in muscle.tag.lower():
                    self.__muscles[muscle.name] = muscle
        return self.__muscles
            
    def build_muscles(self, uses, movingpp):
        self.model.append(ET.Comment('============================== Muscles ==================================='))
        ET.SubElement(self.model, 'muscles').text = 'model://%s/%s.osim' % (self.model_name, self.model_name)
        ET.SubElement(self.model, 'plugin', filename='libgazebo_ros_muscle_interface.so', name='muscle_interface_plugin')
        
        self.movingpp = movingpp
        for x in self.muscles.values():
            use = uses[x.name] if x.name in uses else uses['default']
            if use[:4] == 'true':
                x.adapt(use[4:])
                self.osim_muscles.append(x.omusc)
            
    def build_joints(self, use=[]):
        self.model.append(ET.Comment('============================== Joints ==================================='))
        for body in self.bodies.values():
            if body.joint is not None:
                self.model.append(body.joint.tosdf())

    def build_inclusions(self, inc):
        self.model.append(ET.Comment('============================== Includes ==============================='))
        for x, y in inc.items():
            o = ET.SubElement(self.model, 'include')
            ET.SubElement(o, 'uri').text = 'model://%s' % x
            ET.SubElement(o, 'pose').text = y
            
    def write(self):
        def fromprettyxml(input_xml): #cool name, but not the opposite of dom.toprettyxml()
            output_xml = ''.join([line.strip() for line in input_xml.splitlines()])
            return output_xml

        with open('%s.sdf' % self.newname, 'w') as f:
            f.write(parseString(ET.tostring(self.sdf)).toprettyxml())
        with open('%s.osim' % self.newname, 'w') as f:
            filtered = [x+'\n' for x in parseString(ET.tostring(self.osim)).toprettyxml().split('\n') if not re.match(r'^\s*$', x)]
            f.write(''.join(filtered))

if __name__ == '__main__':    
    parser = argparse.ArgumentParser(description='OSIM to SDF converter')
    parser.add_argument('input', help='path to SDF file')
    parser.add_argument('-c', '--config', help='Conversion configuration file')
    parser.add_argument('-v', '--verbose', help='Enable debug output', action='store_true')
    args = parser.parse_args()

    config = cp.ConfigParser()
    if args.config is not None:
        config.read(args.config)
    else:
        config['general'] = {'model': 'model'}
        config['collisions'] = {'default': 'true'}
        config['joints'] = {'default': 'free'}
        config['muscles'] = {'default': 'true'}
        config['coordinates'] = {}
        config['wraps'] = {}
        config['include'] = {}

    sdf = OSIM(args.input, config['general'], args.verbose)
    sdf.build_body(config['collisions'], config['wraps'], config['joints'])
    sdf.build_joints()
    sdf.build_muscles(config['muscles'], config['coordinates'])
    sdf.build_inclusions(config['include'])
    sdf.write()
